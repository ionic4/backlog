import { Component, OnInit } from '@angular/core';
import { Task } from '../../task';
import { TasksService } from '../tasks.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  inprogress = Array<Task>();
  todo = Array<Task>();
  finished = Array<Task>();

  constructor(public tasksService: TasksService) {}

  ngOnInit() {
    this.inprogress = this.tasksService.inprogress;
    this.todo = this.tasksService.todo;
    this.finished = this.tasksService.finished;
  }

  doReorder(ev: any) {
    ev.detail.complete();
  }

  /*
  private getElementByIndex(index: number) {
    let result: any;
    if (index <= this.inprogress.length) {
      result = this.inprogress[index - 1];
    }
    else if (index <= this.inprogress.length +  this.todo.length + 1 ) {
      result = this.todo[index - this.inprogress.length - 2];
    }
    else  {
      result = this.finished[index - this.inprogress.length - this.todo.length - 3];
    }
    return result;
  }
  */
  
}
